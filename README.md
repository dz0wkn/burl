You can download project from this link: https://github.com/dz0wkn/burl

# Backend
DB configuration in .env file in backend folder need to be modified and run following code
- "php artisan migrate"
Then you should open backend folder in CMD and execute following code
- "composer update"

# frontend
SERVICE URL configure in .env file in frontend folder need to be modified
Then you should open frontend folder in CMD and execute following code
- "npm install"
- "npm run serve"