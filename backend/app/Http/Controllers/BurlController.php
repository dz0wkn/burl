<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Burl;
use Illuminate\Support\Str;

class BurlController extends Controller
{
    // Burl Index action
    public function index(Request $request)
    {
        $burls = Burl::where('expire_date', '>=', date('Y-m-d H:i:s'))->orWhereNull('expire_date')->orderBy('id','desc')->get();
        return $burls;
    }

    // Burl store action
    public function store(Request $request)
    {
        $burl = new Burl;

        if ($request->input('expire_date') == 'one_week') {
            $expire_date = date('Y-m-d H:i:s', strtotime('+7 day', time()));
        } elseif ($request->input('expire_date') == 'one_month') {
            $expire_date = date('Y-m-d H:i:s', strtotime('+1 month', time()));
        } else {
            $expire_date = null;
        }

        $short_url = Str::random(6);
        while (Burl::where('short_url', $short_url)->count() > 0) {
            $short_url = Str::random(6);
        }

        $burl->long_url = $request->input('long_url');
        $burl->short_url = $short_url;
        $burl->expire_date = $expire_date;
        $burl->save();

        return Burl::where('expire_date', '>=', date('Y-m-d H:i:s'))->orWhereNull('expire_date')->orderBy('id','desc')->get();
    }

    public function getLongUrl($key)
    {
        $burl = Burl::where('short_url', $key)->first();
        $burl->clicks += 1;
        $burl->save();
        return $burl->long_url;
    }
}
